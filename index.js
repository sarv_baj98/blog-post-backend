var express = require('express');
// var bodyParser = require('body-parser');
var jwt =require('jsonwebtoken');
const mongoose = require('mongoose');
var bcrypt = require('bcrypt');
const cors = require('cors');
const app =express();
app.use(express.json());
// app.use(require('connect').bodyParser());
// app.use(bodyParser.text({ type: 'text/html' }));
// app.use(bodyParser.text({ type: 'text/xml' }));
// app.use(bodyParser.json({type:'application/*+json'}));
// app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
// const MongoClient = require('mongodb').MongoClient;
// const url = 'mongodb://127.0.0.1:27017'
mongoose.connect('mongodb://localhost:27017/blog-post-data', {useNewUrlParser: true});
const Schema = mongoose.Schema;

let BlogSchema = new Schema({
    title: {type: String, required: true, max: 100},
    dec: {type: String, required: true},
});
let NewUser = new Schema({
  uname:{type:String,required:true},
  pass:{type:String,required:true},
  email:{type:String,required:true},
});
// create application/json parser
// var jsonParser = bodyParser.json();
// create application/x-www-form-urlencoded parser
// var urlencodedParser = bodyParser.urlencoded({ extended: false });

const user = mongoose.model('UserSchema', NewUser);
const blog = mongoose.model('blog', BlogSchema);

// Export the model
// module.exports = mongoose.model('Product', ProductSchema);

mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.get('/api',(req, res)=> {
  res.json({
    Message:"Welcome to initial API!!"
  });
});
app.post('/api/signup', (req,res)=> {
  //auth user
  let keys = Object.keys(req.body);
  let passhash="";
  // bcrypt.genSalt(10, (err, salt) =>{
  //   if (err)
  //     console.log("error from hashing",err);
  //   bcrypt.hash(req.body[keys[1]], salt, function(err, hash) {
  //     // console.log("login pass hash",hash);
  //     passhash=hash;
  //
  //   });
  // });
  let newuser= new user(
    {
        uname:req.body[keys[0]],
        pass:req.body[keys[1]],
        email:req.body[keys[2]]
    }
  );
  newuser.save(function (err) {
        if (err) {
            return err;
        }
        // res.send('New user Created successfully')
        const userdetail= {email:req.body[keys[2]],pass:req.body[keys[1]]};
        const token=jwt.sign({userdetail},'my_secret_key');
        res.json({
          token:token,
          username:req.body[keys[2]]
        })
    })
});

app.post('/api/login',(req,res)=> {
  //auth user
  let keys = Object.keys(req.body);
  console.log(req.body[keys[0]]);
  console.log("login Details==",req.body);
  const userdetail= {email:req.body[keys[0]],pass:req.body[keys[1]]};
  user.findOne({email:req.body[keys[0]]},(err,data)=>{
    if(err){
      res.json({
        text:"User not found"
      });

    }else{
      //due to hashing mismatch
   //    let loginhash='';
   //    // console.log("data from mangodb==",data,typeof(data.pass),typeof(req.body[keys[1]]));
   //    bcrypt.genSalt(10, (err, salt) =>{
   //      if (err)
   //        console.log("error from hashing",err);
   //
   //      bcrypt.hash(req.body[keys[1]], salt, function(err, hash) {
   //        // console.log("login pass hash",hash);
   //        loginhash=hash;
   //
   //      });
   //    });
   //    bcrypt.genSalt(10, (err, salt) =>{
   //      if (err)
   //        console.log("error from hashing",err);
   //
   //      bcrypt.hash(data.pass, salt, function(err, hash) {
   //        console.log("login pass hash from db",hash);
   //        // loginhash=hash;
   //      });
   //    });
   //    bcrypt.compare(req.body[keys[1]], loginhash, function(err, isPasswordMatch) {
   //      console.log("pass match info==",isPasswordMatch,err);
   //     // return err == null ? isPasswordMatch : err;
   // });
      if(req.body[keys[1]]===data.pass){
        console.log("user matched",data.pass);
        const token=jwt.sign({userdetail},'my_secret_key');
        res.json({
          token:token,
          username:data.email
        });
      }
    }
  })
});
app.get('/api/getPost',tokenverify,(req,res)=>{
  jwt.verify(req.token,'my_secret_key',(err,data)=>{
    if(err){
      res.sendStatus(403);
    }else{
      newblog.find({}).toArray(function(err, result) {
        if(err){
          return err;
        }
        res.json({
          data:result
        });
      })
    }
  })
})
app.post('/api/createPost',tokenverify,(req,res)=>{
  jwt.verify(req.token,'my_secret_key',(err,data)=> {
    if(err){
      console.log("123<><> jwt verification 1",req.token,err);
      res.sendStatus(403);
    }else{
      let keys = Object.keys(req.body);
      let newblog= new blog({
        title:req.body[keys[0]],
        dec:req.body[keys[1]]
      }
      );
      newblog.save((err)=>{
        if(err){
          return err;
        }
      })
      blog.find({},(err, result)=> {
      if(err){
        return err;
      }
      res.json({
        data:result
      });
    })


    }
  })
})
app.get('/api/logindetail', tokenverify,(req, res)=>{
  jwt.verify(req.token,'my_secret_key',(err,data)=> {
    if(err){
      res.sendStatus(403);
    }else{
      res.json({
        text:"protected wala phase",
        data:data
      });

    }
  })
});
function tokenverify(req,res,next){
  const bearerHeader=req.headers["authorization"];
  if(typeof bearerHeader !=='undefined'){
    const bearer=bearerHeader.split(" ");
    const bearToken=bearer[1];
    console.log("tokenverify fun==<><><>",bearToken);
    req.token=JSON.parse(bearToken);
    next();
  }else{
    res.sendStatus(403);
  }
}
app.listen(5000,()=> console.log("app listening on port 5000!"));
