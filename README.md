# Blog-post-backend
Project code on integrating MongoDB into Node.js application using Mongoose.

## Run, Build

You'll need [Node.js](https://nodejs.org) installed on your computer in order to run or build this project.
```bash
$ git clone https://gitlab.com/sarv_baj98/blog-post-backend.git
$ cd blog-post-backend
$ npm install
```

Run the project
```bash
$ npm run serve
```
## Some Screenshoot
#
![BlogSchema](https://gitlab.com/sarv_baj98/blog-post-backend/uploads/50b88978d15a60fa65af349ab2aabd92/mongo-blogpost-schema.PNG)
#
![UserSchema](https://gitlab.com/sarv_baj98/blog-post-backend/uploads/511ea747cab56a4a2f8125d216f75786/mongo-user-schema.PNG)
#
![Postman](https://gitlab.com/sarv_baj98/blog-post-backend/uploads/45e76e346a1aee2314af317ac8d1c600/postman.PNG)
